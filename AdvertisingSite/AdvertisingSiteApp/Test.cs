﻿using System;
using System.Linq;

using AdvertisingSiteData.Antities;
using AdvertisingSiteData.Repositories;

namespace AdvertisingSiteApp
{
    public class Test
    {
        private readonly IRepository _repository;
        
        public Test(IRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Добавляет обявление.
        /// </summary>
        void AddAdvert()
        {
            var advert = new Advert()
            {
                SectionAdvertId = 2,
                Heading = "Зонтик",
                Price = 3,
                Description = "Продам зонтик в отличном состоянии.",
                CreationDate = DateTime.Now,
                StatusAdvertId = 1,
                UserId = 1
            };

            Console.WriteLine($"{nameof(AddAdvert)} start");
            var result = _repository.AddAdvert(advert);
            Console.WriteLine($"AdvertId: {result}");
            Console.WriteLine($"{nameof(AddAdvert)} end\n");
        }

        /// <summary>
        /// Добавляет сообщение.
        /// </summary>
        void AddMessage()
        {
            var message = new Message()
            {
                CreationDate = DateTime.Now,
                SenderUserId = 2,
                RecipientUserId = 1,
                Text = "Где можно посмотреть ваши товары?"
            };

            Console.WriteLine($"{nameof(AddMessage)} start");
            var result = _repository.AddMessage(message);
            Console.WriteLine($"MessageId: {result}");
            Console.WriteLine($"{nameof(AddMessage)} end\n");
        }

        /// <summary>
        /// Получает пользователя по логину и паролю.
        /// </summary>
        void FindUser()
        {
            Console.WriteLine($"{nameof(FindUser)} start");
            var result = _repository.GetUserByLogin("user1", "user1");
            Console.WriteLine(
                result == null ? 
                "Not found." : 
                $"User: id {result.Id} nickname {result.Nickname}");
            Console.WriteLine($"{nameof(FindUser)} end\n");
        }

        /// <summary>
        /// Получает все объявления пользователя по разделу.
        /// </summary>
        void GetAllAdvertUser()
        {
            Console.WriteLine($"{nameof(GetAllAdvertUser)} start");
            var result = _repository.GetAllAdvertUserBySectionId(1, 2);
            Console.WriteLine(
                result == null ?
                "Not found." :
                $"Count: {result.Count()}");
            Console.WriteLine($"{nameof(GetAllAdvertUser)} end\n");
        }

        /// <summary>
        /// Получает все отправленные и полученные сообщения пользователя.
        /// </summary>
        void GetMessagesUser()
        {
            Console.WriteLine($"{nameof(GetMessagesUser)} start");
            var result = _repository.GetMessagesUser(2);
            Console.WriteLine(
                result == null ?
                "Not found." :
                $"Count: {result.Count()}");
            Console.WriteLine($"{nameof(GetMessagesUser)} end\n");
        }

        /// <summary>
        /// Получает данные объявления по идентификатору.
        /// </summary>
        void FindAdvert()
        {
            Console.WriteLine($"{nameof(FindAdvert)} start");
            var result = _repository.GetAdvertById(1);
            Console.WriteLine(
                result == null ? 
                "Not found." : 
                $"Advert: id {result.Id} user-nickname {result.User.Nickname}");
            Console.WriteLine($"{nameof(FindAdvert)} end\n");
        }

        public void Run()
        {
            AddAdvert();
            AddMessage();
            FindUser();
            GetAllAdvertUser();
            GetMessagesUser();
            FindAdvert();
        }
    }
}
