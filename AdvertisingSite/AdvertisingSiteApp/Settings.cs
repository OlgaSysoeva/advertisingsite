﻿using System.IO;

using Microsoft.Extensions.Configuration;

namespace AdvertisingSiteApp
{
    public class Settings
    {
        public string DbConnection { get; private set; }

        public Settings()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            
            // создаем конфигурацию
            var config = builder.Build();

            // получаем строку подключения
            DbConnection = config[nameof(DbConnection)];
        }
    }
}
