﻿using System;
using AdvertisingSiteData;
using AdvertisingSiteData.Repositories.ADO;
using AdvertisingSiteData.Repositories.EF;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;

namespace AdvertisingSiteApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri("http://192.168.99.100:9200"))
                {
                    AutoRegisterTemplate = true,
                })
                .CreateLogger();

            Log.Debug("Start app!");

            var settings = new Settings();

            TestEF(settings);
            TestADO(settings);

            Log.Debug("End app!");
        }

        static void TestEF(Settings settings)
        {
            var optionsBuilder = new DbContextOptionsBuilder<AdvertContext>();

            optionsBuilder
                .EnableSensitiveDataLogging()
                .UseLazyLoadingProxies()
                .UseNpgsql(settings.DbConnection);

            using var context = new AdvertContext(optionsBuilder.Options);

            var test = new Test(new EFRepository(context));

            Console.WriteLine("Entity Framework");

            test.Run();
        }

        static void TestADO(Settings settings)
        {
            using var connection = new NpgsqlConnection(settings.DbConnection);

            connection.Open();

            var test = new Test(new AdoRepository(connection));

            Console.WriteLine("ADO");

            test.Run();
        }
    }
}
