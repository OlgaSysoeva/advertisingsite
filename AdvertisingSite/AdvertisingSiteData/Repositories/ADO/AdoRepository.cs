﻿using System.Collections.Generic;
using System.Data;

using AdvertisingSiteData.Antities;

using Npgsql;
using NpgsqlTypes;

namespace AdvertisingSiteData.Repositories.ADO
{
    public class AdoRepository : IRepository
    {
        private readonly NpgsqlConnection _connectionString;

        public AdoRepository(NpgsqlConnection conncetionString)
        {
            _connectionString = conncetionString;
        }

        public int AddAdvert(Advert advert)
        {
            string sqlExpression = "INSERT INTO \"Adverts\" (\"UserId\", \"SectionAdvertId\", " +
                "\"Heading\", \"Price\", \"Description\", \"CreationDate\", \"StatusAdvertId\") " +
                "VALUES (@UserId, @SectionAdvertId, @Heading, @Price, @Description, @CreationDate, " +
                "@StatusAdvertId) RETURNING \"Id\";";
            
            var command = new NpgsqlCommand(sqlExpression, _connectionString);

            command.Parameters.Add(new NpgsqlParameter("@UserId", advert.UserId));
            command.Parameters.Add(new NpgsqlParameter("@SectionAdvertId", advert.SectionAdvertId));
            command.Parameters.Add(new NpgsqlParameter("@Heading", advert.Heading));
            command.Parameters.Add(new NpgsqlParameter("@Price", advert.Price));
            command.Parameters.Add(new NpgsqlParameter("@Description", advert.Description));
            command.Parameters.Add(new NpgsqlParameter("@CreationDate", advert.CreationDate));
            command.Parameters.Add(new NpgsqlParameter("@StatusAdvertId", advert.StatusAdvertId));

            return (int)command.ExecuteScalar();
        }

        public int AddMessage(Message message)
        {
            string sqlExpression = "INSERT INTO \"Messages\" (\"SenderUserId\", " +
                "\"RecipientUserId\", \"Text\", \"CreationDate\")" +
                "VALUES (@SenderUserId, @RecipientUserId, @Text, @CreationDate) RETURNING \"Id\";";
            
            var command = new NpgsqlCommand(sqlExpression, _connectionString);

            command.Parameters.Add(new NpgsqlParameter("@SenderUserId", message.SenderUserId));
            command.Parameters.Add(new NpgsqlParameter("@RecipientUserId", message.RecipientUserId));
            command.Parameters.Add(new NpgsqlParameter("@Text", message.Text));
            command.Parameters.Add(new NpgsqlParameter("@CreationDate", message.CreationDate));

            return (int)command.ExecuteScalar();
        }

        public Advert GetAdvertById(int advertId)
        {
           string sqlExpression = "SELECT \"Adverts\".\"Id\", \"Users\".\"Nickname\" " +
                "FROM \"Adverts\" JOIN \"Users\" ON \"Adverts\".\"UserId\" = \"Users\".\"Id\" " +
                "WHERE \"Adverts\".\"Id\"=@advertId " +
                "LIMIT 1";

            var command = new NpgsqlCommand(sqlExpression, _connectionString);

            command.Parameters.Add(new NpgsqlParameter("@advertId", advertId));

            var reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    return new Advert()
                    {
                        Id = reader.GetInt32(0),
                        User = new User()
                        {
                            Nickname = reader.GetString(1)
                        }
                    };
                }
            }

            reader.Close();
            return null;
        }

        public IEnumerable<Advert> GetAllAdvertUserBySectionId(int userId, int sectionId)
        {
            string sqlExpression = "SELECT \"Adverts\".\"Id\" " +
                "FROM \"Adverts\" " +
                "WHERE \"Adverts\".\"UserId\"=@userId AND \"Adverts\".\"SectionAdvertId\"=@sectionId";

            var command = new NpgsqlCommand(sqlExpression, _connectionString);

            command.Parameters.Add(new NpgsqlParameter("@UserId", userId));
            command.Parameters.Add(new NpgsqlParameter("@sectionId", sectionId));

            var reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                var adverts = new List<Advert>();

                while (reader.Read())
                {
                    adverts.Add(new Advert()
                    {
                        Id = reader.GetInt32(0),
                    });
                }

                return adverts;
            }

            reader.Close();
            return null;
        }

        public IEnumerable<Message> GetMessagesUser(int userId)
        {
            string sqlExpression = "SELECT \"Messages\".\"Id\" " +
                "FROM \"Messages\" " +
                "WHERE \"Messages\".\"SenderUserId\"=@userId " +
                "OR \"Messages\".\"RecipientUserId\"=@userId";

            var command = new NpgsqlCommand(sqlExpression, _connectionString);

            command.Parameters.Add(new NpgsqlParameter("@UserId", userId));

            var reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                var messages = new List<Message>();

                while (reader.Read())
                {
                    messages.Add(new Message()
                    {
                        Id = reader.GetInt32(0),
                    });
                }

                return messages;
            }

            reader.Close();
            return null;
        }

        public User GetUserByLogin(string login, string password)
        {
            string sqlExpression = "SELECT \"Users\".\"Id\", \"Users\".\"Nickname\" " +
                "FROM \"Users\" " +
                "WHERE \"Users\".\"Login\"=@login AND \"Users\".\"Password\"=@password " +
                "AND \"Users\".\"Relevance\"=true " +
                "LIMIT 1";

            var command = new NpgsqlCommand(sqlExpression, _connectionString);

            command.Parameters.Add(new NpgsqlParameter("@login", login));
            command.Parameters.Add(new NpgsqlParameter("@password", password));

            var reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    return new User()
                    {
                        Id = reader.GetInt32(0),
                        Nickname = reader.GetString(1)
                    };
                }
            }

            reader.Close();
            return null;
        }   
    }
}
