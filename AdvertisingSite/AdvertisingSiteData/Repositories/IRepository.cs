﻿using AdvertisingSiteData.Antities;
using System.Collections.Generic;

namespace AdvertisingSiteData.Repositories
{
    public interface IRepository
    {
        /// <summary>
        /// Добавляет обявление.
        /// </summary>
        /// <param name="advert">Модель объявления.</param>
        int AddAdvert(Advert advert);

        /// <summary>
        /// Добавляет сообщение.
        /// </summary>
        /// <param name="message">Модель сообщения.</param>
        int AddMessage(Message message);

        /// <summary>
        /// Получает пользователя по логину и паролю.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <param name="password">Пароль пользователя.</param>
        /// <returns>Возвращает данные пользователя.</returns>
        User GetUserByLogin(string login, string password);

        /// <summary>
        /// Получает все объявления пользователя по разделу.
        /// </summary>
        /// <param name="userId">Идентификатор пользователя.</param>
        /// <param name="sectionId">Идентификатор раздела.</param>
        /// <returns>Возвращает список объявлений.</returns>
        IEnumerable<Advert> GetAllAdvertUserBySectionId(int userId, int sectionId);

        /// <summary>
        /// Получает все отправленные и полученные сообщения пользователя.
        /// </summary>
        /// <param name="userId">Идентификатор пользователя.</param>
        /// <returns>Возвращает список сообщений.</returns>
        IEnumerable<Message> GetMessagesUser(int userId);

        /// <summary>
        /// Получает данные объявления по идентификатору.
        /// </summary>
        /// <param name="advertId">Идентификатор объявления.</param>
        /// <returns>Возвращает данные объявления.</returns>
        Advert GetAdvertById(int advertId);
    }
}
