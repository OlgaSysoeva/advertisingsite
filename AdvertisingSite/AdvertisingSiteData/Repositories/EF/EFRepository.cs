﻿using System.Collections.Generic;
using System.Linq;

using AdvertisingSiteData.Antities;

namespace AdvertisingSiteData.Repositories.EF
{
    public class EFRepository : IRepository
    {
        private AdvertContext _context;

        public EFRepository(AdvertContext context)
        {
            _context = context;
        }

        public int AddAdvert(Advert advert)
        {
            var result = _context.Adverts.Add(advert);
            _context.SaveChanges();
            return result.Entity.Id;

        }

        public int AddMessage(Message message)
        {
            var result = _context.Messages.Add(message);
            _context.SaveChanges();
            return result.Entity.Id;
        }

       public User GetUserByLogin(string login, string password) =>
            _context.Users
                .Where(x => x.Login == login && x.Password == password && x.Relevance == true)
                .FirstOrDefault();
        
        public IEnumerable<Advert> GetAllAdvertUserBySectionId(int userId, int sectionId) =>
            _context.Adverts
                .Where(x => x.UserId == userId && x.SectionAdvertId == sectionId)
                .ToList();

        public IEnumerable<Message> GetMessagesUser(int userId) =>
            _context.Messages
                .Where(x => x.SenderUserId == userId || x.RecipientUserId == userId)
                .ToList();

        public Advert GetAdvertById(int advertId) =>
            _context.Adverts
                .Where(x => x.Id == advertId)
                .FirstOrDefault();
    }
}
