﻿using AdvertisingSiteData.Antities;

using Microsoft.EntityFrameworkCore;

namespace AdvertisingSiteData
{
    public class AdvertContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<StatusAdvert> StatusAdverts { get; set; }
        public DbSet<TypeAdvert> TypeAdverts { get; set; }
        public DbSet<SectionAdvert> SectionAdverts { get; set; }
        public DbSet<Advert> Adverts { get; set; }
        public DbSet<Message> Messages { get; set; }


        public AdvertContext(DbContextOptions<AdvertContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User() 
                { 
                    Id = 1, 
                    Nickname  = "user1", 
                    Login  = "user1", 
                    Password = "user1", 
                    Email = "user1@gmail.com"
                },
                new User() 
                { 
                    Id = 2, 
                    Nickname = "user2", 
                    Login = "user2", 
                    Password = "user2", 
                    Email = "user2@gmail.com" 
                }
                );

            modelBuilder.Entity<StatusAdvert>().HasData(
                new StatusAdvert() { Id = 1, Name = "Активно"},
                new StatusAdvert() { Id = 2, Name = "Не активно" },
                new StatusAdvert() { Id = 3, Name = "Продано" },
                new StatusAdvert() { Id = 4, Name = "Удалено" }
                );

            modelBuilder.Entity<TypeAdvert>().HasData(
                new TypeAdvert() { Id = 1, Name = "Продукт" },
                new TypeAdvert() { Id = 2, Name = "Услуга" }
                );

            modelBuilder.Entity<SectionAdvert>().HasData(
                new SectionAdvert() { Id = 1, Name = "Личные вещи", TypeAdvertId = 1 },
                new SectionAdvert() { Id = 2, Name = "Аксессуары", ParentId = 1, TypeAdvertId = 1 },
                new SectionAdvert() { Id = 3, Name = "Бытовые услуги", TypeAdvertId = 2 },
                new SectionAdvert() { Id = 4, Name = "Химчистка", ParentId = 3, TypeAdvertId = 2 }
                );

            base.OnModelCreating(modelBuilder);
        }
    }
}
