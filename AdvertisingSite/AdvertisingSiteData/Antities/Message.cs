﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdvertisingSiteData.Antities
{
    /// <summary>
    /// Сообщения между пользователями по объявлениям.
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Идентификатор сообщения.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор пользователя отправлителя.
        /// </summary>
        public int SenderUserId { get; set; }

        /// <summary>
        /// Идентификатор пользователя получателя.
        /// </summary>
        public int RecipientUserId { get; set; }

        /// <summary>
        /// Текст сообщения.
        /// </summary>
        [Required]
        public string Text { get; set; }

        /// <summary>
        /// Дата написания сообщения.
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Ссылка на пользователя отправителя.
        /// </summary>
        [ForeignKey(nameof(SenderUserId))]
        public virtual User SenderUser { get; set; }

        /// <summary>
        /// Ссылка на пользователя получателя.
        /// </summary>
        [ForeignKey(nameof(RecipientUserId))]
        public virtual User RecipientUser { get; set; }
    }
}
