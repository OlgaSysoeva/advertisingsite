﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdvertisingSiteData.Antities
{
    /// <summary>
    /// Статус объявлений.
    /// </summary>
    public class StatusAdvert
    {
        /// <summary>
        /// Идентификатор объявления.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Наименование статуса объявления.
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}
