﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdvertisingSiteData.Antities
{
    /// <summary>
    /// Разделы продутов и услуг.
    /// </summary>
    public class SectionAdvert
    {
        /// <summary>
        /// Идентификатор раздела.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Наименование раздела.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Родительский идентификатор, если раздел вложен.
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// Идетификатор типа объявления.
        /// </summary>
        public int TypeAdvertId { get; set; }

        /// <summary>
        /// Ссылка на связанный тип объявления.
        /// </summary>
        [ForeignKey(nameof(TypeAdvertId))]
        public virtual TypeAdvert TypeAdvert { get; set; }
    }
}
