﻿using System.ComponentModel.DataAnnotations;

namespace AdvertisingSiteData.Antities
{
    /// <summary>
    /// Сущность для хранения данных о пользователях.
    /// </summary>
    public class User
    {
        public User()
        {
            Relevance = true;
        }

        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Общевидимое имя пользователя.
        /// </summary>
        [Required]
        public string Nickname { get; set; }

        /// <summary>
        /// Логин для авториации.
        /// </summary>
        [Required]
        public string Login { get; set; }

        /// <summary>
        /// Пароль для авторизации.
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// Электронная почта.
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Статус актуальности пользователя.
        /// </summary>
        public bool Relevance { get; set; }
    }
}
