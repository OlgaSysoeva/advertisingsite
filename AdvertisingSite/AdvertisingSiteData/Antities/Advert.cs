﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdvertisingSiteData.Antities
{
    /// <summary>
    /// Объявления пользователей.
    /// </summary>
    public class Advert
    {
        /// <summary>
        /// Идентификатор объявления.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Связанный идентификатор пользователя-автора.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Идентификатор раздела, в котором находится объявление.
        /// </summary>
        public int SectionAdvertId { get; set; }

        /// <summary>
        /// Текстовый заголовок объявления.
        /// </summary>
        [Required]
        public string Heading { get; set; }

        /// <summary>
        /// Стоимость предлагаемого товара/услуги.
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// Описание объявления.
        /// </summary>
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Дата создания объявления.
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Идентификатор статуса объявления.
        /// </summary>
        public int StatusAdvertId { get; set; }

        /// <summary>
        /// Ссылка на пользователя - владельца.
        /// </summary>
        [ForeignKey(nameof(UserId))]
        public virtual User User { get; set; }

        /// <summary>
        /// Ссылка на связанный раздел.
        /// </summary>
        [ForeignKey(nameof(SectionAdvertId))]
        public virtual SectionAdvert SectionAdvert { get; set; }

        /// <summary>
        /// Ссылка на статус объявления.
        /// </summary>
        [ForeignKey(nameof(StatusAdvertId))]
        public virtual StatusAdvert StatusAdvert { get; set; }
    }
}
