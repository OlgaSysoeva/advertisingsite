﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdvertisingSiteData.Antities
{
    /// <summary>
    /// Тип объявления.
    /// </summary>
    public class TypeAdvert
    {
        /// <summary>
        /// Идентификатор типа объявления.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Назание типа объявления.
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}
